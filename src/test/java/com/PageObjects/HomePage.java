package com.PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class HomePage {

	WebDriver ldriver;
	WebDriver driver = null;
	
	
	public HomePage(WebDriver rdriver){
		
		this.ldriver=rdriver;
		
		PageFactory.initElements(rdriver, this);
		
	}
	
	@FindBy(xpath = "//input[@id='name']")
	@CacheLookup
	WebElement NameInputField;
	
	@FindBy(xpath = "//input[@id='email']")
	@CacheLookup
	WebElement EmailInputField;
	
	@FindBy(xpath = "//input[@id='phone']")
	@CacheLookup
	WebElement PhoneNumberInputField;
	
	@FindBy(xpath = "//input[@id='subject']")
	@CacheLookup
	WebElement SubjectInputField;
	
	@FindBy(xpath = "//textarea[@id='description']")
	@CacheLookup
	WebElement BodyMessageInputField;
	
	@FindBy(id = "submitContact")
	@CacheLookup
	WebElement BtnSubmitContact;
	
	
	
	public void EnterUsername(String name) {
		NameInputField.clear();
		NameInputField.sendKeys(name);
	}
	
	public void EnterUserEmail(String email) {
		EmailInputField.clear();
		EmailInputField.sendKeys(email);
	}
	
	public void EnterPhoneNumber(String phone) {
		PhoneNumberInputField.clear();
		PhoneNumberInputField.sendKeys(phone);
	}
	public void EnterSubject(String subject) {
		SubjectInputField.clear();
		SubjectInputField.sendKeys(subject);
	}
	
	public void EnterBodyMessage(String message) {
		BodyMessageInputField.clear();
		BodyMessageInputField.sendKeys(message);
	}
	
	public void ClickBtnSubmit() {
		BtnSubmitContact.click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void VerifyResposeMessage(String text) throws InterruptedException {
		
		String actualResponseText = driver.findElement(By.cssSelector("div:nth-child(1) > h2")).getText();
		Assert.assertEquals(actualResponseText, text);
		Thread.sleep(3000);
	}
	
	
}
