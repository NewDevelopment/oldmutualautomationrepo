package com.TestCases;

import org.testng.annotations.Test;

import com.PageObjects.HomePage;

public class TC_AutomationTestCase_001 extends BaseClass{
	
	@Test
	
	public void SubmitQueryTest() {
		try {
			driver.get(BaseUrl);
			HomePage homepage =new HomePage(driver);
			homepage.EnterUsername(name);
			homepage.EnterUserEmail(email);
			homepage.EnterPhoneNumber(phone);
			homepage.EnterSubject(subject);
			homepage.EnterBodyMessage(message);
			homepage.VerifyResposeMessage("Thanks for getting in touch Onke Ngcatsha!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
