package com.TestCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import com.Utilities.ReadConfig;

public class BaseClass {
	ReadConfig readconfig = new ReadConfig();

	WebDriver driver = null;
	public String BaseUrl = readconfig.getApplicationBaseUrl();
	public String name = readconfig.getName();
	public String email = readconfig.getEmail();
	public String phone = readconfig.getPhone();
	public String subject = readconfig.getSubject();
	public String message = readconfig.getMessage();
	public String FrameworkPath = System.getProperty("user.dir");

	@Parameters("browser")
	@BeforeTest

	public void setUp(String br) {

		if (br.equals("Firefox")) {
			System.setProperty("webdriver.gecko.driver", FrameworkPath + readconfig.getFirefoxDriver());
			driver = new FirefoxDriver();
			driver.manage().window().maximize();

		}
		else if (br.equals("Chrome")) {
			System.setProperty("webdriver.gecko.driver",FrameworkPath +readconfig.getChomeDriver());
			driver = new ChromeDriver();
			driver.manage().window().maximize();
		}

	}

	@AfterTest
	public void tearDown() {
		driver.quit();
	}

}
