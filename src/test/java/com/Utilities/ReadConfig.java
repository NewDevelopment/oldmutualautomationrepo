package com.Utilities;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ReadConfig {
	Properties pro;

	public ReadConfig() {

		File src = new File("./Configurations/Config.properties");
		try {
			FileInputStream fis = new FileInputStream(src);
			pro = new Properties();
			pro.load(fis);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getApplicationBaseUrl() {

		String url = pro.getProperty("BaseUrl");
		return url;
	}

	public String getFirefoxDriver() {

		String ffDriver = pro.getProperty("Firefox");
		return ffDriver;
	}

	public String getChomeDriver() {

		String chromeDriver = pro.getProperty("Chrome");
		return chromeDriver;
	}
	
	public String getName() {

		String name = pro.getProperty("Name");
		return name;
	}
	public String getEmail() {

		String email = pro.getProperty("Email");
		return email;
	}
	
	public String getPhone() {

		String phone = pro.getProperty("Phone");
		return phone;
	}
	
	public String getSubject() {

		String subject = pro.getProperty("Subject");
		return subject;
	}
	
	public String getMessage() {

		String message = pro.getProperty("Message");
		return message;
	}
	
	

}
